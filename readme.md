# College Spam Checker - Version 1.2.0
Python script to show you who's sending the most mail in a folder - Now with multi-threaded capabilities!

# Setup
Python 3 is required to run CollegeSpamChecker. 3.5 and above is recommended, 3.4 and below may have issues when running CSC.

You'll need these extra libraries:
* PrettyTable
* click

If you're on linux you'll need the `python3-tk` package for the chart to display, use your preferred package manager to install this.

You can go into the Releases section and download v1.2.0. Unzip the file and go into the csc directory. 

You can also use git to clone this repository using this command:

`git clone https://gitlab.com/o355/collegespamchecker.git`

Then change directory into csc (either using `cd csc` or `dir csc`)

# Operation
Run main.py with Python 3

Enter the address of the mail server you need to connect to, then enter your credentials, then select the folder. Select how many threads you want CollegeSpamChecker to use.

Processing ~3000 emails takes about 30 seconds when using 20 threads.

# Known issues
Gmail & Yahoo Mail support is finicky and requires you to allow insecure applications. CollegeSpamChecker will provide links before and after analysis so you can turn the setting on/off.