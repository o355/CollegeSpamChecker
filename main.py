# CollegeSpamChecker v1.2.0

import sys
import imaplib
import getpass
import email
import concurrent
from concurrent.futures import ThreadPoolExecutor
from threading import Lock
import math
import re

lock = Lock()
senderdict = {}


def mail_download(numbers):
    global username, password, folder, bar
    client = imaplib.IMAP4_SSL(server)
    client.login(username, password)
    client.select(folder)
    for i in numbers:
        rv, data = client.fetch(i, '(RFC822)')
        msg = email.message_from_bytes(data[0][1])
        sender = msg['From']
        sender = sender.split("<")[0]
        sender = re.sub('\r\n', '', sender)
        sender = re.sub('"', '', sender)

        lock.acquire()
        try:
            senderdict[sender] += 1
        except KeyError:
            senderdict[sender] = 1

        bar.update(1)

        lock.release()

    client.close()

try:
    import click
except ImportError:
    print("No module found named click, please install before using CollegeSpamChecker")

try:
    from prettytable import PrettyTable
except ImportError:
    print("No module found named prettytable, please install before using CollegeSpamChecker")

table = PrettyTable()
table.field_names = ["Sender", "Number"]

print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
print("Welcome to CollegeSpamChecker - Version 1.2.0")
print("Please enter the email server that you'd like to connect to below.")
print("You can also enter Gmail, Yahoo Mail, or Outlook.com for pre-configured settings.")
server = input("Enter here: ")

print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
if server.lower() == "gmail" or server.lower() == "imap.gmail.com":
    print("Please note: Allow less secure apps must be enabled for CollegeSpamChecker to work with Gmail.")
    print("You can enable this setting here: https://myaccount.google.com/lesssecureapps, then login.")
    server = "imap.gmail.com"
elif server.lower() == "yahoo mail" or server.lower() == "imap.mail.yahoo.com":
    print("Please note: Allow less secure apps must be enabled for CollegeSpamChecker to work with Yahoo Mail.")
    print("You can enable this setting here: https://login.yahoo.com/account/security#less-secure-apps, then login.")
    server = "imap.mail.yahoo.com"
elif server.lower() == "outlook.com":
    server = "outlook.office365.com"

print("Logging into server: %s" % server)
try:
    M = imaplib.IMAP4_SSL(server)
except:
    print("Failed to connect to %s. Make sure the server is online, and supports IMAP4 over SSL." % server)
    sys.exit()

senderdict = {}
username = input("Enter username: ")
password = getpass.getpass()
try:
    M.login(username, password)
except imaplib.IMAP4.error:
    print("Failed to log into server - An IMAP4 error occurred.")
    print("Check credentials, and make sure the server is online.")
    sys.exit()

folder_index = []

status, folder_list = M.list()
for i in M.list()[1]:
    l = i.decode().split(' "." ')
    folder_index.append(l[1])

print("Please select which folder you'd like to check for spam.")
print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
for x, i in enumerate(folder_index, 0):
    print("%s: %s" % (x, re.sub('"', '', i)))

print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
folder_index_input = input("Enter here: ")

if folder_index_input == "":
    print("A number was not selected, please relaunch CollegeSpamChecker and try again.")
    sys.exit()

try:
    folder_index_input = int(folder_index_input)
except ValueError:
    print("A number was not entered, please ensure you enter a number.")
    sys.exit()

try:
    folder = folder_index[folder_index_input]
except IndexError:
    print("You selected a number that was out of index. Select a number that is in the index.")
    sys.exit()

rv, data = M.select(folder)

if rv != 'OK':
    print("There was an issue selecting the folder. Please try again later.")
    sys.exit()

print("Please select the number of threads you'd like to check for spam.")
print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
print("0: 1 thread - Very slow")
print("1: 2 threads - Slow")
print("2: 5 threads - Default")
print("3: 10 threads - Fast - May cause throttling issues on old email systems.")
print("4: 20 threads - Very fast - Could cause throttling issues.")
print("5: 40 threads - Hyper fast - Will likely cause throttling issues.")
print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
thread_count = input("Enter here: ")
if thread_count == "0":
    max_workers = 1
elif thread_count == "1":
    max_workers = 2
elif thread_count == "2":
    max_workers = 5
elif thread_count == "3":
    max_workers = 10
elif thread_count == "4":
    max_workers = 20
elif thread_count == "5":
    max_workers = 40
else:
    max_workers = 5

messages = str(data)
messages = str(messages.replace("b", "").replace("[", "").replace("]", "").replace("'", ""))
print("Processing %s messages, please be patient." % messages)
rv, data = M.search(None, "ALL")
if rv != 'OK':
    print("No messages found.")
    sys.exit()

M.close()
mailids = data[0].split()

chunks = math.ceil(len(mailids) / max_workers)
bar = click.progressbar(length=len(mailids), show_pos=True, label="Processing mail")
executor = ThreadPoolExecutor(max_workers=max_workers)
futures = [executor.submit(mail_download, mailids[i*chunks:(i+1)*chunks]) for i in range(max_workers)]

for future in concurrent.futures.as_completed(futures):
    try:
        result = future.result()
    except:
        executor.shutdown()
        print("\n\n\n")
        print("                      !!!!! YOU (PROBABLY) SET YOUR THREAD COUNT TOO HIGH !!!!!                     ")
        print("  IMAPLib has encountered a fatal error, likely due to too many concurrent connections/throttling.  ")
        print("                You may have lost your internet connection, make sure it's working.                 ")
        print("                      Try lowering your thread count, then wait a few minutes.                      ")
        print("                CollegeSpamChecker cannot complete the analysis, and is now closing.                ")
        print("\n\n\n")
        sys.exit()

print("\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
senderdict2 = sorted(senderdict, key=senderdict.get, reverse=True)
for sender_name in senderdict2:
    sender_count = senderdict[sender_name]
    table.add_row([sender_name.replace("''", "").replace('""', ''), sender_count])

print(table)
if server.lower() == "imap.gmail.com":
    print("Please note: It is a good idea to turn off allow less secure apps now that analysis is done.")
    print("You can disable the setting here: https://myaccount.google.com/lesssecureapps")
elif server.lower() == "imap.mail.yahoo.com":
    print("Please note: It is a good idea to turn off allow less secure apps now that analysis is done.")
    print("You can disable the setting here: https://login.yahoo.com/account/security#less-secure-apps")
