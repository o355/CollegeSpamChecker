# CollegeSpamChecker changelog
This is the changelog for all releases of CollegeSpamChecker.

## Version 1.2.0 - Released on October 14, 2019
* Multi-threaded functionality has been added. CollegeSpamChecker is now much faster.
* At the input prompt you can enter Gmail, Yahoo Mail, or Outlook.com to use one of these services with server autofill.
* CollegeSpamChecker now prompts you if you're using Gmail or Yahoo Mail to allow less secure apps, and will provide links to turn this setting on and off before and after analysis.
* Removed excess code from matplotlib.
* Removed the config file.
* Fixed issues with the table having bad formatting for certain senders.
* Fixed two UI issues - Folder names won't have quotation marks if there is a space, and the separator between the progress bar and the table is printed with a newline.
* The progress bar now shows the number of email items that have been processed instead of a percent.